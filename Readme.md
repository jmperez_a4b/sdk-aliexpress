# SDK Aliexpress

SDK Aliexpress

 - Nos logueamos en este enlace:
https://login.aliexpress.com/?return=https%3A%2F%2Flogin.aliexpress.com%2FiLogin.htm%3Fgoto%3Dhttp%253A%252F%252Fconsole.open.taobao.com%252F%26active%3Dtrue
_`(Usuario y contraseña en el passpack)`_

 - Seleccionamos la app de la que queremos obtener el SDK

 - En el menu de la izquierda seleccionamos **"SDK Download"**

 - Seleccionamos **"Php version"**

 - Si no hay ningún SDK disponible debemos pulsar **"You can also regenerate the SDK. Click to generate"**

 - Recargamos la página pasados unos minutos y descargamos la última versión del SDK **"PHP version last generation time: 2018-10-16 22:28:14 click to download"**